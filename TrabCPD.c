#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define size 100000

void bubble_sort(int vetor[], int tam){
	int aux, i, j;
	for(j=tam-1; j>=1; j--){
		for(i=0; i<j; i++){
			if(vetor[i]>vetor[i+1]){
				aux=vetor[i];
                    vetor[i]=vetor[i+1];
                    vetor[i+1]=aux;
            }
        }
    }
}

void selection_sort (int vetor[], int tam){
	int i, j, min, aux;
  
  	for (i = 0; i < (tam - 1); i++) {
    	min = i;
    	for (j = i+1; j < tam; j++)
    		if (vetor[j] < vetor[min])
				min = j;

	    if (i != min) {
    	  	aux = vetor[i];
      		vetor[i] = vetor[min];
      		vetor[min] = aux;
    	}
  	}
}

void insertion_sort(int vetor[], int tam){
    int i, key, j;
    for (i = 1; i < tam; i++) {
        key = vetor[i];
        j = i - 1;
        while (j >= 0 && vetor[j] > key) {
            vetor[j + 1] = vetor[j];
            j = j - 1;
        }
        vetor[j + 1] = key;
    }
}

void quick_sort(int vetor[], int left, int right){
    int i, j, x, y;
     
    i = left;
    j = right;
    x = vetor[(left + right) / 2];
     
    while(i <= j) {
        while(vetor[i] < x && i < right)
			i++;
        while(vetor[j] > x && j > left)
			j--;
			
        if(i <= j) {
            y = vetor[i];
            vetor[i] = vetor[j];
            vetor[j] = y;
            i++;
            j--;
        }
    }
     
    if(j > left)
        quick_sort(vetor, left, j);
    if(i < right)
        quick_sort(vetor, i, right);
}

void merge(int vetor[], int comeco, int meio, int fim) {
    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;
    int *vetAux;
    vetAux = (int*)malloc(tam * sizeof(int));

    while(com1 <= meio && com2 <= fim){
        if(vetor[com1] < vetor[com2]) {
            vetAux[comAux] = vetor[com1];
            com1++;
        } else {
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }

    while(com1 <= meio){  //Caso ainda haja elementos na primeira metade
        vetAux[comAux] = vetor[com1];
        comAux++;
        com1++;
    }

    while(com2 <= fim) {   //Caso ainda haja elementos na segunda metade
        vetAux[comAux] = vetor[com2];
        comAux++;
        com2++;
    }

    for(comAux = comeco; comAux <= fim; comAux++){    //Move os elementos de volta para o vetor original
        vetor[comAux] = vetAux[comAux-comeco];
    }
    
    free(vetAux);
}

void merge_sort(int vetor[], int comeco, int fim){
    if (comeco < fim) {
        int meio = (fim+comeco)/2;

        merge_sort(vetor, comeco, meio);
        merge_sort(vetor, meio+1, fim);
        merge(vetor, comeco, meio, fim);
    }
}

void heap_sort(int vetor[], int tam) {
   	int i = tam/2, pai, filho, t;
   	
	while(1) {
		if (i > 0) {
    		i--;
    	  	t = vetor[i];
  		}else{
      		tam--;
      		if (tam <= 0) 
				return;
      		t = vetor[tam];
      		vetor[tam] = vetor[0];
  		}
 		pai = i;
  		filho = i * 2 + 1;
  		
		while (filho < tam) {
      		if ((filho + 1 < tam)  &&  (vetor[filho + 1] > vetor[filho]))
          		filho++;
      		if (vetor[filho] > t) {
        		vetor[pai] = vetor[filho];
         		pai = filho;
         		filho = pai * 2 + 1;
      		}else
         		break;
  		}
  		vetor[pai] = t;
	}	
}

void copyArray(int a[], int b[]){
	for(int i=0;i<size;i++)
		b[i] = a[i];
}

void reverseArray(int a[], int b[]){
	for(int i=0, j=size-1;i<size;i++, j--)
		b[j] = a[i];
}

void listaArray(int vetor[]){
	for(int i=0;i<size;i++)
		printf("%d ", vetor[i]);
	printf("\n\n");
}

void aleatorio_decrescente(int original[], int aux[]){
	clock_t tp_inicio, tp_fim;
	
	printf("Array ordenado apos bubble sort - ");
	tp_inicio = clock();
	bubble_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	copyArray(original, aux);
	printf("\n\n");
	
	printf("Array ordenado apos selection sort - ");
	tp_inicio = clock();
	selection_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	copyArray(original, aux);
	printf("\n\n");
	
	printf("Array ordenado apos insertion sort - ");
	tp_inicio = clock();
	insertion_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	copyArray(original, aux);
	printf("\n\n");
	
	printf("Array ordenado apos quicksort sort - ");
	tp_inicio = clock();
	quick_sort(aux, 0, size-1);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	copyArray(original, aux);
	printf("\n\n");
	
	printf("Array ordenado apos mergesort - ");
	tp_inicio = clock();
	merge_sort(aux, 0, size-1);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	copyArray(original, aux);
	printf("\n\n");
	
	printf("Array ordenado apos heapsort - ");
	tp_inicio = clock();
	heap_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
}

void crescente(int aux[]){
	clock_t tp_inicio, tp_fim;
	
	printf("Array ordenado apos bubble sort - ");
	tp_inicio = clock();
	bubble_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	printf("\n\n");
	
	printf("Array ordenado apos selection sort - ");
	tp_inicio = clock();
	selection_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	printf("\n\n");
	
	printf("Array ordenado apos insertion sort - ");
	tp_inicio = clock();
	insertion_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	printf("\n\n");
	
	printf("Array ordenado apos quicksort sort - ");
	tp_inicio = clock();
	quick_sort(aux, 0, size-1);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	printf("\n\n");
	
	printf("Array ordenado apos mergesort - ");
	tp_inicio = clock();
	merge_sort(aux, 0, size-1);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
	
	printf("\n\n");
	
	printf("Array ordenado apos heapsort - ");
	tp_inicio = clock();
	heap_sort(aux, size);
	tp_fim = clock();
	printf("Tempo: %lf",(tp_fim - tp_inicio) / (double)CLOCKS_PER_SEC);
}

int main(){
	srand(time(NULL));
	
	int original[size], aux[size]; 
	
	//gera numeros aleat�rios
	for(int i=0;i<size;i++)
		original[i] = aux[i] = rand();
	
	//roda todos no aleat�rio
	printf("Foram ordenados %d valores aleatorios\n\n", size);
	aleatorio_decrescente(original, aux); 
	
	//roda todos no crescente aproveitando que o vetor aux est� oredenado pelo passo anterior (aleat�rio)
	printf("\n\nForam ordenados %d valores inicialmente em ordem crescente\n\n", size);
	crescente(aux);
	
	//roda todos no decrescente
	printf("\n\nForam ordenados %d valores inicialmente em ordem decrescente\n\n", size);
	//inverte o array aux(ordenado anteriormente) em original, ficando este ultimo em forma decrescente
	reverseArray(aux, original);
	//deixa ambos os arrays invertidos para servir de referencia quando executar os passos
	copyArray(original, aux); 	 
	//ordena o array aux e dps usa o original para voltar a forma invertida e executar o proximo algoritmo
	aleatorio_decrescente(original, aux);
	
	printf("\n\n");
	system("pause");
	return 0;
}
